package com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SonartestProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(SonartestProjectApplication.class, args);
	}

}
